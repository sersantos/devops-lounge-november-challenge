# Description

This repo is made of 3 main parts:

The packer build responsible for creating 2 ubuntu AWS AMI and trigger the ansible playbooks. One of the AMI is going to be responsible for hosting the wordpress and his dependencies and the other the mysql server.
Requires `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` enviroment vars.
    
The ansible playbooks made of 4 different roles: wordpress, mysql, php and apache.

At last the gitlab pipeline responsible for executing the above tasks.

NOTE: The packer build runs fine locally, but when using gitlab pipeline is failing because of some issue related with the apt lock. Unfortunately 
I was running into the AWS free tier snapshot space and had to stop the debuggin there since I wasn't being able to fix the aws problem even after deleting the snapshots.
